import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SceneTest {

    @org.junit.jupiter.api.Test
    void getHeight() {
        Scene scene = new Scene(0,0);
        assertEquals(0, scene.getHeight());

        Scene scene2 = new Scene(10,0);
        assertEquals(10, scene2.getHeight());
    }

    @org.junit.jupiter.api.Test
    void getWidth(){
        Scene scene = new Scene(0,0);
        assertEquals(0, scene.getWidth());

        Scene scene2 = new Scene(0,10);
        assertEquals(10, scene2.getWidth());

    }
    @org.junit.jupiter.api.Test
    public void testAddTerrain() {
        Scene scene = new Scene (5, 3);
        scene.addTerrain("G", 2, 1);
        scene.addTerrain("T", 4, 2);
        assertEquals("G", scene.getTerrain(2, 1));
        assertEquals("T", scene.getTerrain(4, 2));
    }
    Scene scene = new Scene(3, 5);

    @BeforeEach
    void setUp() {

        scene.addTerrain(null, 0, 0);
        scene.addTerrain(null, 1, 0);
        scene.addTerrain(null, 2,0);

        scene.addTerrain("C", 0, 1);
        scene.addTerrain(null, 1, 1);
        scene.addTerrain("A", 2,1);

        scene.addTerrain("B", 0, 2);
        scene.addTerrain(null, 1, 2);
        scene.addTerrain(null, 2,2);

        scene.addTerrain(null, 0, 3);
        scene.addTerrain(null, 1, 3);
        scene.addTerrain(null, 2,3);

        scene.addTerrain(null, 0, 4);
        scene.addTerrain(null, 1, 4);
        scene.addTerrain("D", 2,4);
    }

    @org.junit.jupiter.api.Test
    void getDistanceBetweenTerrains() {
        assertEquals(1, scene.getDistanceBetweenTerrains(0,0, 0, 1));
        assertEquals(2, scene.getDistanceBetweenTerrains(0,0, 0, 2));
        assertEquals(3, scene.getDistanceBetweenTerrains(0,0, 2, 1));
        assertEquals(6, scene.getDistanceBetweenTerrains(0,0, 2, 4));

        assertEquals(1, scene.getDistanceBetweenTerrains(0,1, 0, 0));
        assertEquals(2, scene.getDistanceBetweenTerrains(0,2, 0, 0));
        assertEquals(3, scene.getDistanceBetweenTerrains(2,1, 0, 0));
        assertEquals(6, scene.getDistanceBetweenTerrains(2,4, 0, 0));
    }

    @org.junit.jupiter.api.Test
    void isEmpty() {
        assertEquals(true, scene.isEmpty(0,0));
        assertEquals(true, scene.isEmpty(1,1));
        assertEquals(false, scene.isEmpty(2,1));
        assertEquals(false, scene.isEmpty(0,1));
    }

    @org.junit.jupiter.api.Test
    void countItems() {
        assertEquals(0, scene.countItems(0,0,0));
        assertEquals(1, scene.countItems(0,0,1));
        assertEquals(3, scene.countItems(0,0,3));
       // assertEquals(2, scene.countItems(0,0,2));
        assertEquals(4, scene.countItems(0,0,6));
    }

    @Test
    void findByValue() {
        assertEquals(false, scene.findByValue("e"));
        assertEquals(false, scene.findByValue(null));
        assertEquals(false, scene.findByValue("g"));
        assertEquals(true, scene.findByValue("A"));
        assertEquals(true, scene.findByValue("B"));
        assertEquals(true, scene.findByValue("C"));
        assertEquals(true, scene.findByValue("D"));
    }
}