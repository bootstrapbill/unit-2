public class Scene {

    private int height;
    private int width;
    private String[][] terrain;

    public Scene(int height, int width) {
        this.height = height;
        this.width = width;
        this.terrain = new String[height][width];
    }

    public int getHeight (){
        return height;
    }

    public int getWidth (){
        return width;
    }

    public void addTerrain(String terrain, int row, int column) {
        this.terrain[row][column] = terrain;
    }

    public int getDistanceBetweenTerrains(int rowA, int columnA, int rowB, int columnB) {

        int rowDistance = rowB - rowA;
        int columnDistance = columnB - columnA;

        return Math.abs(rowDistance + columnDistance);
    }

    public boolean isEmpty(int row, int column) {
        return terrain[row][column] == null;
    }

    public int countItems(int row, int column, int maxDistance) {
        int count = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (!isEmpty(i, j)) {
                    if (getDistanceBetweenTerrains(row, column, i, j) <= maxDistance) {
                        count += 1;
                    }
                }
            }
        }

        return count;
    }

    public boolean findByValue(String value){
        boolean isPresent = false;

        if(value == null){
            return false;
        }

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if(terrain[i][j] == value){
                    isPresent = true;
                }
            }
        }

        return isPresent;
    }

    public String getTerrain(int row, int column) {
        return terrain[row][column];
    }

}
