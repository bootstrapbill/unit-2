public class Main {

    public static void main(String[] args){

        Scene scene = new Scene(3, 5);

        scene.addTerrain(null, 0, 0);
        scene.addTerrain(null, 1, 0);
        scene.addTerrain(null, 2,0);

        scene.addTerrain("C", 0, 1);
        scene.addTerrain(null, 1, 1);
        scene.addTerrain("A", 2,1);

        scene.addTerrain(null, 0, 2);
        scene.addTerrain(null, 1, 2);
        scene.addTerrain("B", 2,2);

        scene.addTerrain(null, 0, 3);
        scene.addTerrain(null, 1, 3);
        scene.addTerrain(null, 2,3);

        scene.addTerrain("D", 0, 4);
        scene.addTerrain(null, 1, 4);
        scene.addTerrain(null, 2,4);

        System.out.println(scene.getTerrain(1,1));
        System.out.println(scene.isEmpty(1,1));
    }
}
